Dado("que eu acesso o site das lojas Americanas") do
    visit 'http://americanas.com'
end

Quando("clico na barra de busca") do
    find(:css, input[id:'h_search-input']).click
end

E("quando pesquiso por Perfumes") do
    first(:css, "a[href*='id_category=11']").click
    set 'Perfumes'
end

E("clico na lupa - botão de pesquisa") do
    first(:css, click_button[id='h_search-btn'])
end

Então("verifico o retorno da pesquisa na página") do    
    expect(page).to h1(class='h1 page-title')
end

E("clico no primeiro produto") do
    click_button('internacional')
    click_button('comprar')
end

E("clico no botão minha cesta") do
    find(:css, [id='icon-cart']).click
end

E("verifico se o produto está na cesta") do
    expect(page).to have_selector("Quantidade: 1")
end
   
