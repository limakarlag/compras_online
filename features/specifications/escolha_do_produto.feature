#language: pt

Funcionalidade: Escolher um produto para comprar
    Como usuário do sistema
    Eu quero escolher o produto que desejo comprar 

Cenário: Acessa a página da loja
    Dado que eu acesso o site das lojas Americanas
    Quando clico na barra de busca
    E quando pesquiso por Perfumes
    E clico na lupa - botão de pesquisa
    Então verifico o retorno da pesquisa na página
    E clico no primeiro produto
    E clico no botão minha cesta
    E verifico se o produto está na cesta
